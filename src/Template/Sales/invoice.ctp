<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;


?><!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 4.0.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Invoice - Mara Xpress | Dear System API Integration App</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="<?= $this->Url->build('/', true); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->Url->build('/', true); ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->Url->build('/', true); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->Url->build('/', true); ?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
        <link href="<?= $this->Url->build('/', true); ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= $this->Url->build('/', true); ?>assets/admin/invoice.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->Url->build('/', true); ?>assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>

        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?= $this->Url->build('/', true); ?>assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->Url->build('/', true); ?>assets/global/css/plugins-md.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->Url->build('/', true); ?>assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="<?= $this->Url->build('/', true); ?>assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->Url->build('/', true); ?>assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-md page-header-fixed page-sidebar-closed-hide-logo ">
        <!-- BEGIN HEADER -->
        <?php echo $this->element('header'); ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php echo $this->element('sidebar'); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN PAGE HEAD -->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>Invoice <small></small></h1>
                        </div>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                        <div class="page-toolbar">
                            <!-- BEGIN THEME PANEL -->
                            <!-- END THEME PANEL -->
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD -->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Sales</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="#">Invoice</a>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="portlet light">
                        <div class="portlet-body">
                            <div class="invoice">
                                <div class="row invoice-logo">
                                    <div class="col-xs-6 invoice-logo-space">
                                        <img src="<?= $this->Url->build('/', true); ?>assets/global/img/walmart.png" class="img-responsive" alt=""/>
                                    </div>
                                    <div class="col-xs-6">
                                        <p>
                                            #<?php
                                            if(isset($orderID)){
                                                echo($orderID);
                                            }else{
                                                echo $message;
                                            }
                                            ?> / 28 Feb 2013 <span class="muted">
                                            Consectetuer adipiscing elit </span>
                                        </p>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <h3>Client:</h3>
                                        <ul class="list-unstyled">
                                            <li>
                                                John Doe
                                            </li>
                                            <li>
                                                Mr Nilson Otto
                                            </li>
                                            <li>
                                                FoodMaster Ltd
                                            </li>
                                            <li>
                                                Madrid
                                            </li>
                                            <li>
                                                Spain
                                            </li>
                                            <li>
                                                1982 OOP
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-4">
                                        <h3>About:</h3>
                                        <ul class="list-unstyled">
                                            <li>
                                                Drem psum dolor sit amet
                                            </li>
                                            <li>
                                                Laoreet dolore magna
                                            </li>
                                            <li>
                                                Consectetuer adipiscing elit
                                            </li>
                                            <li>
                                                Magna aliquam tincidunt erat volutpat
                                            </li>
                                            <li>
                                                Olor sit amet adipiscing eli
                                            </li>
                                            <li>
                                                Laoreet dolore magna
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-4 invoice-payment">
                                        <h3>Payment Details:</h3>
                                        <ul class="list-unstyled">
                                            <li>
                                                <strong>V.A.T Reg #:</strong> 542554(DEMO)78
                                            </li>
                                            <li>
                                                <strong>Account Name:</strong> FoodMaster Ltd
                                            </li>
                                            <li>
                                                <strong>SWIFT code:</strong> 45454DEMO545DEMO
                                            </li>
                                            <li>
                                                <strong>V.A.T Reg #:</strong> 542554(DEMO)78
                                            </li>
                                            <li>
                                                <strong>Account Name:</strong> FoodMaster Ltd
                                            </li>
                                            <li>
                                                <strong>SWIFT code:</strong> 45454DEMO545DEMO
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Item
                                                    </th>
                                                    <th class="hidden-480">
                                                        Description
                                                    </th>
                                                    <th class="hidden-480">
                                                        Quantity
                                                    </th>
                                                    <th class="hidden-480">
                                                        Unit Cost
                                                    </th>
                                                    <th>
                                                        Total
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        Hardware
                                                    </td>
                                                    <td class="hidden-480">
                                                        Server hardware purchase
                                                    </td>
                                                    <td class="hidden-480">
                                                        32
                                                    </td>
                                                    <td class="hidden-480">
                                                        $75
                                                    </td>
                                                    <td>
                                                        $2152
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        Furniture
                                                    </td>
                                                    <td class="hidden-480">
                                                        Office furniture purchase
                                                    </td>
                                                    <td class="hidden-480">
                                                        15
                                                    </td>
                                                    <td class="hidden-480">
                                                        $169
                                                    </td>
                                                    <td>
                                                        $4169
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        Foods
                                                    </td>
                                                    <td class="hidden-480">
                                                        Company Anual Dinner Catering
                                                    </td>
                                                    <td class="hidden-480">
                                                        69
                                                    </td>
                                                    <td class="hidden-480">
                                                        $49
                                                    </td>
                                                    <td>
                                                        $1260
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        Software
                                                    </td>
                                                    <td class="hidden-480">
                                                        Payment for Jan 2013
                                                    </td>
                                                    <td class="hidden-480">
                                                        149
                                                    </td>
                                                    <td class="hidden-480">
                                                        $12
                                                    </td>
                                                    <td>
                                                        $866
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="well">
                                            <address>
                                                <strong>Loop, Inc.</strong><br/>
                                                795 Park Ave, Suite 120<br/>
                                                San Francisco, CA 94107<br/>
                                                <abbr title="Phone">P:</abbr> (234) 145-1810 </address>
                                            <address>
                                                <strong>Full Name</strong><br/>
                                                <a href="mailto:#">
                                                    first.last@email.com </a>
                                            </address>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 invoice-block">
                                        <ul class="list-unstyled amounts">
                                            <li>
                                                <strong>Sub - Total amount:</strong> $9265
                                            </li>
                                            <li>
                                                <strong>Discount:</strong> 12.9%
                                            </li>
                                            <li>
                                                <strong>VAT:</strong> -----
                                            </li>
                                            <li>
                                                <strong>Grand Total:</strong> $12489
                                            </li>
                                        </ul>
                                        <br/>
                                        <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();">
                                            Print <i class="fa fa-print"></i>
                                        </a>
                                        <!-- <a class="btn btn-lg green hidden-print margin-bottom-5">
Submit Your Invoice <i class="fa fa-check"></i>
</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php echo $this->element('footer'); ?>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
<script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/respond.min.js"></script>
<script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
        <!-- END CORE PLUGINS -->
        <script src="<?= $this->Url->build('/', true); ?>assets/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
        <script src="<?= $this->Url->build('/', true); ?>assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
        <script>
            jQuery(document).ready(function() {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                Demo.init(); // init demo features


                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }

                    <?php

                if($viewResponse == "success"){
                    echo "toastr.success('$message', 'Confirmation');";
                }else{
                    echo "toastr.error('$message', 'Error');";
                }

                    ?>
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
