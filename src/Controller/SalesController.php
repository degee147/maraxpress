<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Http\Client;
/**
 * Sales Controller
 *
 * @property \App\Model\Table\SalesTable $Sales
 */
class SalesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

    }

    public function getOrders()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://inventory.dearsystems.com/ExternalApi/SaleList",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "api-auth-accountid: 735bc830-3b91-4473-8776-fb15cbc6e433",
            "api-auth-applicationkey: 89e88974-029b-1c05-1ac3-fcd65c2fe97a",
            "cache-control: no-cache",
            "postman-token: a449dd94-ec31-dce3-1f3c-404d0ea2f7db"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {

            $data = json_decode($response, true);

            // $iTotalRecords = 178;
            $iTotalRecords = $data['Total'];
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


            for($i = $iDisplayStart; $i < $end; $i++) {
                //$id = ($i + 1);
                $records["data"][] = array(
                    //'<input type="checkbox" name="id[]" value="'.$id.'">',
                    //$id,
                    $data['SaleList'][$i]['OrderNumber'],
                    $data['SaleList'][$i]['OrderDate'],
                    $data['SaleList'][$i]['Customer'],
                    $data['SaleList'][$i]['CustomerReference'],
                    $data['SaleList'][$i]['InvoiceNumber'],
                    $data['SaleList'][$i]['ShipBy'],
                    $data['SaleList'][$i]['PaidAmount'],
                    '<a href="/sales/invoice/'.$data['SaleList'][$i]['ID'].'" class="btn btn-xs default"><i class="fa fa-search"></i> Invoice</a>',
                );
            }

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            $this->response->body(json_encode($records));

        }
    }



    public function invoice($ID)
    {

       // $this->autoRender = false;
        if(empty($ID)){
            return $this->redirect(['action' => 'index']);
        }else{
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://inventory.dearsystems.com/ExternalApi/Sale/$ID",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                "api-auth-accountid: 735bc830-3b91-4473-8776-fb15cbc6e433",
                "api-auth-applicationkey: 89e88974-029b-1c05-1ac3-fcd65c2fe97a",
                "cache-control: no-cache",
                "postman-token: a449dd94-ec31-dce3-1f3c-404d0ea2f7db"
            ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $data = json_decode($response, true);

                $postData = array(
                    'orderID' => $data['ID'],
                    'customerAddress' => $data['ShippingAddress']['DisplayAddressLine1'] . ", ". $data['ShippingAddress']['DisplayAddressLine2'],
                    'area' => !empty($data['ShippingAddress']['Postcode']) ? $data['ShippingAddress']['Postcode'] : "Null",
                    'customerCity' => !empty($data['ShippingAddress']['City']) ? $data['ShippingAddress']['City'] : "Abu Dhabi",
                    'customerContact' => !empty($data['Phone']) ? $data['Phone'] : "00000000",
                    'customerEmail' => !empty($data['Email']) ? $data['Email'] : "Null",
                    'customerName' => $data['Customer'],
                    'orderAmount' => $data['Order']['Lines'][0]['AverageCost'],
                    'orderType' => "forward",
                    'paymentMethod' => $data['Invoice']['Status'] == "PAID" ? "prepaid" : "cod",
                    'comments' => "Thanks for order",
                    'productDescription' => "product 1, product 2"
                );


                $curl2 = curl_init();

                curl_setopt_array($curl2, array(
                    CURLOPT_URL => "http://api.maraxpress.com/orders?apiKey=495294ad9e2841d8b27909cdc2f03608",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($postData),
                    CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "postman-token: 56c18bc8-434d-a1e0-d1b8-a7b463fabbd6"
                ),
                ));

                $response2 = curl_exec($curl2);
                $err2 = curl_error($curl2);

                curl_close($curl2);

                if ($err2) {
                    echo "cURL Error #:" . $err2;
                } else {

                    $data2 = json_decode($response2, true);

                    if(isset($data2['success'])){

                        $this->set('viewResponse', "success");
                        $this->set('message', $data2['success']['message']);
                        $this->set('httpStatusCode', $data2['success']['httpStatusCode']);
                        $this->set('orderID', $data2['success']['orderID']);


                    }elseif(isset($data2['error'])){

                        $this->set('viewResponse', "error");
                        $this->set('message', $data2['error']['message']);
                        $this->set('httpStatusCode', $data2['error']['httpStatusCode']);

                    }
                }
            }
        }
    }
}
