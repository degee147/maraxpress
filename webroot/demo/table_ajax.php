<?php
/*
   * Paging
   */


$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://inventory.dearsystems.com/ExternalApi/SaleList",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
    "api-auth-accountid: 735bc830-3b91-4473-8776-fb15cbc6e433",
    "api-auth-applicationkey: 89e88974-029b-1c05-1ac3-fcd65c2fe97a",
    "cache-control: no-cache",
    "postman-token: a449dd94-ec31-dce3-1f3c-404d0ea2f7db"
),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}

/*
$iTotalRecords = 178;
$iDisplayLength = intval($_REQUEST['length']);
$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
$iDisplayStart = intval($_REQUEST['start']);
$sEcho = intval($_REQUEST['draw']);

$records = array();
$records["data"] = array();

$end = $iDisplayStart + $iDisplayLength;
$end = $end > $iTotalRecords ? $iTotalRecords : $end;

$status_list = array(
    array("success" => "Pending"),
    array("info" => "Closed"),
    array("danger" => "On Hold"),
    array("warning" => "Fraud")
);

for($i = $iDisplayStart; $i < $end; $i++) {
    $status = $status_list[rand(0, 2)];
    $id = ($i + 1);
    $records["data"][] = array(
        //'<input type="checkbox" name="id[]" value="'.$id.'">',
        $id,
        '12/09/2013',
        'Jhon Doe',
        'Jhon Doe',
        '450.60$',
        rand(1, 10),
        '<span class="label label-sm label-'.(key($status)).'">'.(current($status)).'</span>',
        '<a href="javascript:;" class="btn btn-xs default"><i class="fa fa-search"></i> View</a>',
    );
}

if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
    $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
    $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
}

$records["draw"] = $sEcho;
$records["recordsTotal"] = $iTotalRecords;
$records["recordsFiltered"] = $iTotalRecords;*/

//echo json_encode($records);
?>
